<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <section class="main">

                <?php include('inc/sidebar.inc.php') ?>

                <section class="content">
                    <div class="content__wrap">

                        <div class="inner">

                            <h1>Участники операции</h1>

                            <nav class="menu">
                                <ul>
                                    <li>
                                        <a href="#" title="Открыть">
                                            <i>
                                                <img src="images/icon__open.png" alt="Открыть">
                                            </i>
                                            <span>Открыть</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="Открыть">
                                            <i>
                                                <img src="images/icon__add.png" alt="Добавить">
                                            </i>
                                            <span>Добавить</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="Удалить">
                                            <i>
                                                <img src="images/icon__remove.png" alt="Удалить">
                                            </i>
                                            <span>Удалить</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="Товары">
                                            <i>
                                                <img src="images/icon__product.png" alt="Товары">
                                            </i>
                                            <span>Товары</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>

                            <div class="content__main">

                                <div class="data_table"></div>

                            </div>

                        </div>

                    </div>
                </section>

            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

        <script>

            var employees = [
                { id: '1', name: "Общество с ограниченной ответсвенностью \"Технологическая компания Шлюмберже\"", type: "", data: "" },
                { id: '2', name: "Общество с ограниченной ответсвенностью \"компания Шлюмберже\"", type: "", data: ""  },
                { id: '3', name: "Общество с ограниченной ответсвенностью \"Технологическая компания Шлюмберже\"", type: "", data: ""  },
                { id: '4', name: "Общество с ограниченной ответсвенностью \"компания Шлюмберже\"", type: "", data: ""  }
            ];

            $(function(){
                var i1 = 0;

                $(".data_table").dxTreeList({
                    dataSource: employees,
                    columnAutoWidth: true,
                    wordWrapEnabled: true,
                    showBorders: true,
                    selection: {
                        mode: "single",
                        recursive: false
                    },
                    columns: [
                        { dataField: "id", caption: "№", width: 55 },
                        { dataField: "name", caption: "Полное наименование" },
                        { dataField: "type", caption: "Вид участника" },
                        {
                            dataField: "data",
                            caption: "Данные",
                            width: 100,
                            alignment: 'center',
                            cellTemplate: function(container, options) {

                                i1 = i1 + 1;
                                var checkboxOne = 'checkbox_' + i1;
                                var checkOne = '.' + checkboxOne;

                                container
                                    .append($("<div>", { "class": checkboxOne}))
                                    .append("\n");
                                $(checkOne).dxCheckBox({});
                            },
                        },
                    ]
                });
            });

        </script>

    </body>
</html>
