<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <section class="main">

                <?php include('inc/sidebar.inc.php') ?>

                <section class="content">
                    <div class="content__wrap">

                        <div class="inner">
                            <h1>Шаблон</h1>
                        </div>

                        <nav class="menu">
                            <ul>
                                <li>
                                    <a href="#" title="Открыть">
                                        <i>
                                            <img src="images/icon__open.png" alt="Открыть">
                                        </i>
                                        <span>Открыть</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="Открыть">
                                        <i>
                                            <img src="images/icon__add.png" alt="Добавить">
                                        </i>
                                        <span>Добавить</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="Удалить">
                                        <i>
                                            <img src="images/icon__remove.png" alt="Удалить">
                                        </i>
                                        <span>Удалить</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="Товары">
                                        <i>
                                            <img src="images/icon__product.png" alt="Товары">
                                        </i>
                                        <span>Товары</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>

                    </div>
                </section>

            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

    </body>
</html>
