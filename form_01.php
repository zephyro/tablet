<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="page">

            <section class="main forms">

                <div class="container">
                    <div class="row">

                        <div class="col-lg-10 offset-lg-1 col-xl-8 offset-lg-2">
                            <div class="forms__logo">
                                Информационная система<br/>
                                экспортного контроля
                            </div>

                            <div class="text_lead">
                                Для осуществления регистрации неоходимо иметь сертификат
                                усиленной квалификации электронной подписи
                            </div>

                            <div class="warning_block mb_30">
                                Если сертификат отсутствует, его можно получить в одном из удостоверяющих
                                центров, аккредитованных в ФТС
                            </div>

                            <div class="table_light data_table mb_40"></div>

                            <div class="forms__inner">
                                <form class="form">
                                    <label class="form_label">E-mail</label>
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-8">
                                            <div class="form-group">
                                                <div class="input_01"></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-6 col-sm-4">
                                            <div class="form-group">
                                                <div class="button_01"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form_label">Мобильный телефон</label>
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-8">
                                                <div class="input_02"></div>
                                            </div>
                                            <div class="col-xs-6 col-sm-4"></div>
                                        </div>
                                    </div>
                                    <div class="form-group mb_30">
                                        <div class="checkbox_01"></div>
                                    </div>
                                    <div class="text-center">
                                        <div class="button_02"></div>
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>


                </div>


            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

        <script>

            $(".input_01").dxTextBox({});

            $(".input_02").dxTextBox({});

            $(".button_01").dxButton({
                elementAttr: {
                    class: "btn_border"
                },
                "text": "Заполнить из УКЭП"
            });

            $(".button_02").dxButton({
                elementAttr: {
                    class: "btn_green"
                },
                "text": "Регистрация"
            });

            $(".checkbox_01").dxCheckBox({
                text: "Я ознакомился исогласен с пользовательским соглашением и уведомлением о правилах использования персональных данных",
            });

            var employees = [
                { id: "1", companyName: "ООО фламинго", inn: "ИНН: 7010237725", userName: "Иванов Иван Иванович", workTime: "с 10.12.18 по 11.12.21"},
                { id: "2", companyName: "ООО Промбанк", inn: "ИНН: 7010221755", userName: "Сидоров Иван Васильевич", workTime: "с 04.04.18 по 11.12.20"},
                { id: "3", companyName: "ООО Зима", inn: "ИНН: 7010237845", userName: "Петров Евгений Петрович", workTime: "с 23.04.18 по 09.12.22"},
                { id: "4", companyName: "ООО Стройинвест", inn: "ИНН: 7010237771", userName: "Иванов Сергей Иванович", workTime: "с 15.10.17 по 05.08.20"},
                { id: "5", companyName: "", inn: "", userName: "", workTime: ""},
                { id: "6", companyName: "", inn: "", userName: "", workTime: ""}
            ];

            $(function(){
                $(".data_table").dxTreeList({
                    dataSource: employees,
                    columnAutoWidth: true,
                    wordWrapEnabled: true,
                    showBorders: true,
                    selection: {
                        mode: "single",
                        recursive: false
                    },
                    columns: [
                        "companyName",
                        "inn",
                        "userName",
                        "workTime"
                    ],
                });
            });

        </script>

    </body>
</html>

