<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <section class="main">

                <?php include('inc/sidebar.inc.php') ?>
                
                <section class="content">
                    <div class="content__wrap">

                        <div class="inner">

                            <ul class="breadcrumbs">
                                <li><a href="#">Анализ товаров</a></li>
                                <li><a href="#">Объект № 1</a></li>
                                <li><a href="#">Контрольные списки</a></li>
                                <li><span>Сравнительный анализ</span></li>
                            </ul>

                            <nav class="menu menu_border">
                                <ul>
                                    <li>
                                        <a href="#" title="Открыть">
                                            <i>
                                                <img src="images/icon__exit.png" alt="Открыть">
                                            </i>
                                            <span>Список</span>
                                        </a>
                                    </li>

                                </ul>
                            </nav>

                            <div class="form_group mb_40">
                                <div class="textarea_01"></div>
                            </div>

                            <div class="form_group mb_40">
                                <div class="textarea_02"></div>
                            </div>

                            <div class="main_row mb_40">
                                <div class="main_row__primary">
                                    <div class="box">
                                        <div class="box__heading"><span>Технические характеристики для сравнения</span></div>
                                        <div class="textarea_03"></div>
                                    </div>
                                </div>
                                <div class="main_row__second">
                                    <div class="status">
                                        <div class="status__item">
                                            <div class="button_01"></div>
                                        </div>
                                        <div class="status__item">
                                            <div class="button_02"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>
                </section>

            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

        <script>

            $(".textarea_01").dxTextArea({
                height: 465,
                disabled: true,
                value: 'Здесь должно быть описание пункта контрольного списка, выбранного на шаре ранее. Описание может быть ооооочень большим, с вертикальной прокруткой. \n' +
                    'Не редактируемое поле'
            });

            $(".textarea_02").dxTextArea({
                height: 110,
                disabled: true,
                value: 'Техническое описание товара, которое пользователь ввел на этапе описания данного товара. Не редактируемое поле.'
            });

            $(".textarea_03").dxTextArea({
                height: 70,
                disabled: true,
                placeholder: 'Техническое описание, которое вводит пользователь. Редактируемое поле',
            });

            $(".button_01").dxButton({
                elementAttr: {
                    class: "btn_switch"
                },
                "text": "Соответствует"
            });

            $(".button_02").dxButton({
                elementAttr: {
                    class: "btn_switch"
                },
                "text": "Не соответствует"
            });

        </script>

    </body>
</html>
