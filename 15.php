<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <section class="main">

                <?php include('inc/sidebar.inc.php') ?>
                
                <section class="content">
                    <div class="content__wrap">

                        <div class="inner">

                            <h1>Характер внешнеэкономической операции</h1>

                            <nav class="menu menu_border">
                                <ul>
                                    <li>
                                        <a href="#" title="Вернуться к анализу">
                                            <i>
                                                <img src="images/icon__exit.png" alt="Вернуться к анализу">
                                            </i>
                                            <span>Вернуться к анализу</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="Сформировать заключение">
                                            <i>
                                                <img src="images/icon__generate.png" alt="Открыть">
                                            </i>
                                            <span>Сформировать заключение</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>

                            <div class="data_table table_box mb_40"></div>

                            <div class="text-right color_green">Выбрать из списка документов</div>

                            <div class="box mb_5">
                                <div class="box__heading"><span>Документы</span></div>
                                <div class="inline_block">
                                    <div class="inline_block__item">
                                        <div class="form_inline form_inline_float form_inline_long mb_10">
                                            <div class="form_inline__label">Наименование документа</div>
                                            <div class="form_inline__input">
                                                <div class="input_01"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inline_block__item">
                                        <div class="form_inline form_inline_float form_inline_long mb_10">
                                            <div class="form_inline__label">Рег. №</div>
                                            <div class="form_inline__input">
                                                <div class="input_02"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inline_block__item">
                                        <div class="form_inline form_inline_float mb_10">
                                            <div class="form_inline__label">Рег. дата</div>
                                            <div class="form_inline__input">
                                                <div class="date_01"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inline_block__item">
                                        <div class="upload_01"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="mb_30">
                                <a href="#" class="btn_text">Добавить</a>
                            </div>

                        </div>

                    </div>
                </section>

            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

        <script>

            var employees = [
                {
                    id: "1",
                    radio: '',
                    text: "Пункт выбора",
                },
                {
                    id: "2",
                    radio: '',
                    text: "Пункт выбора",
                },
                {
                    id: "3",
                    radio: '',
                    text: "Пункт выбора",
                },
                {
                    id: "4",
                    radio: '',
                    text: "Пункт выбора",
                },
                {
                    id: "5",
                    radio: '',
                    text: "Пункт выбора",
                },
                {
                    id: "6",
                    radio: '',
                    text: "Пункт выбора",
                },
            ];

            $(function(){

                var i1 = 0;

                $(".data_table").dxTreeList({
                    dataSource: employees,
                    columnAutoWidth: true,
                    wordWrapEnabled: true,
                    showBorders: true,
                    selection: {
                        mode: "single",
                        recursive: false
                    },
                    columns: [
                        {
                            dataField: "radio",
                            caption: " ",
                            width: 55,
                            alignment: 'center',
                            cellTemplate: function(container, options) {

                                i1 = i1 + 1;

                                var radio = '<span class="radio_emu"></span>';

                                container
                                    .append(radio);
                            },
                        },
                        {
                            dataField: "text",
                            caption: "Описание внешнеэкономической операции"
                        }
                    ]
                });
            });

            $(".input_01").dxTextBox({});

            $(".input_02").dxTextBox({});

            $(".date_01").dxDateBox({});

            $(".upload_01").dxFileUploader({
                selectButtonText: 'Файл',
                showFileList: false,
                labelText: ''
            });


        </script>

    </body>
</html>
