<footer class="footer">
    <div class="footer__row">
        <div class="footer__text">Сервис соответствует Правилам идентификации, утвержденным постановлением Правительства РФ № 565</div>
        <a class="footer__support" href="tel:88005554420">
            <span>Техническая поддержка</span>
            <strong>8 800 555 44 20</strong>
        </a>
    </div>
</footer>