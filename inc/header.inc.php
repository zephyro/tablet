<header class="header">
    <div class="header__row">
        <ul class="header__nav">
            <li>
                <div class="header__menu" title="Главная"></div>
            </li>
            <li>
                <div class="header__home" title="Главная"></div>
            </li>
            <li>
                <div class="header__save" title="Сохранить"></div>
            </li>
        </ul>
        <div class="header__logo">
            <i><img src="images/logo.png" class="img-fluid" alt=""></i>
            <span>Информационный сервис<br/>экспортного контроля</span>
        </div>
    </div>
</header>