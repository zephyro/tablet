<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <section class="main">

                <?php include('inc/sidebar.inc.php') ?>

                <section class="content">
                    <div class="content__wrap">

                        <div class="inner">

                            <ul class="breadcrumbs">
                                <li><a href="#">Анализ участников</a></li>
                                <li><a href="#">Иностранные участники</a></li>
                                <li><a href="#">Объект № 1</a></li>
                                <li><span>Запреты и ограничения</span></li>
                            </ul>

                            <nav class="menu menu_border">
                                <ul>
                                    <li>
                                        <a href="#" title="Открыть">
                                            <i>
                                                <img src="images/icon__exit.png" alt="Открыть">
                                            </i>
                                            <span>Список</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="На контроле">
                                            <i>
                                                <img src="images/icon__control.png" alt="На контроле">
                                            </i>
                                            <span>На контроле</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>

                            <div class="info_bar">
                                <div class="info_bar__elem">
                                    <i><img src="images/icon__loop_check.png" alt=""></i>
                                    <span>Связи отсутсвуют</span>
                                </div>
                            </div>

                            <div class="search mb_20">
                                <div class="form_line">
                                    <div class="form_line__elem form_line__long">
                                        <div class="input_01"></div>
                                    </div>
                                    <div class="form_line__elem">
                                        <div class="button_01"></div>
                                    </div>
                                    <div class="form_line__elem">
                                        <div class="button_02"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="base_table mb_40"></div>

                            <div class="status_bar">
                                <div class="status_bar__elem">
                                    <i><img src="images/icon__alert.png" alt=""></i>
                                    <span>Запреты и ограничения отсутсвуют</span>
                                </div>
                            </div>

                        </div>

                    </div>
                </section>

            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

        <script>
            var employees = [
                { id: "1", name: "Кабельный удлинниетль PEDMT, из 3-х медных проводник...", data: "", doc: ""},
                { id: "2", name: "Электродвигатель асинхронный переменного тока 117, 17... ", data: "", doc: ""},
                { id: "3", name: "Электродвигатель асинхронный переменного тока 117, 20...", data: "", doc: ""},
                { id: "4", name: "Оборудование фильтровальное: модульная гидрозащита...", data: "", doc: ""},
                { id: "5", name: "Оборудование фильтровальное: модульная гидрозащита...", data: "", doc: ""},
                { id: "6", name: "Изделия из пластмасс: уплотнительная прокладка, для...", data: "", doc: ""},
                { id: "7", name: "Части жидностных насосов: рабочее колесо (крыльчатка)...", data: "", doc: ""},
                { id: "8", name: "Части жидностных насосов: рабочее колесо (крыльчатка)...", data: "", doc: ""},
                { id: "9", name: "Машины и механические устройства имеющие индивид...", data: "", doc: ""}
            ];

            $(function(){
                $(".base_table").dxTreeList({
                    dataSource: employees,
                    columnAutoWidth: true,
                    wordWrapEnabled: true,
                    showBorders: true,
                    selection: {
                        mode: "multiple",
                        recursive: false
                    },
                    columns: [
                        { dataField: "id", caption: "", width: 55 },
                        { dataField: "name", caption: "Наименование" },
                        { dataField: "data", caption: "Дополнительные сведения" },
                        { dataField: "doc", caption: "Документ основание" }
                    ]
                });
            });

            $(".input_01").dxTextBox({});

            $(".button_01").dxButton({
                "text": "Поиск"
            });

            $(".button_02").dxButton({
                "text": "Очистить"
            });

        </script>

    </body>
</html>
