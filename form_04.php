<!doctype html>
<html class="no-js" lang="">

<?php include('inc/head.inc.php') ?>

<body>

<div class="page">

    <section class="main forms">

        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-10 offset-lg-1 col-xl-8 offset-lg-2">

                    <div class="forms__logo">
                        Информационная система<br/>
                        экспортного контроля
                    </div>

                    <div class="forms__thanks">
                        <i><img src="images/icon__mail.png" alt=""></i>
                        <span>Благодарим за регистрацию!</span>
                    </div>

                    <div class="forms__text">
                        На адрес электронной почты <a href="mailto:afanaskina.as@gmail.com ">afanaskina.as@gmail.com </a> отправлена ссылка и код для активации учетной записи<br/>
                        Перейдите по ссылке в письме или введите код в поле ниже
                    </div>

                    <div class="forms__form mb_20">
                        <div class="forms__form_item">
                            <div class="input_01"></div>
                        </div>
                        <div class="forms__form_item">
                            <div class="button_01"></div>
                        </div>
                    </div>

                    <div class="text-center mb_10">Письмо должно прийи на вашу почту в течении 10 минут. </div>
                    <div class="text-center"><a href="#"><strong>Письмо не пришло?</strong></a></div>

                </div>
            </div>
        </div>

    </section>

    <?php include('inc/footer.inc.php') ?>

</div>

<?php include('inc/scripts.inc.php') ?>

<script>

    $(".input_01").dxTextBox({});

    $(".button_01").dxButton({
        elementAttr: {
            class: "btn_green"
        },
        "text": "Активировать учетную запись"
    });

</script>

</body>
</html>
