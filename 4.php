<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <section class="main">

                <?php include('inc/sidebar.inc.php') ?>

                <section class="content">
                    <div class="content__wrap">

                        <div class="inner">

                            <h1>Сведение об участнике операции</h1>

                            <nav class="menu menu_border">
                                <ul>
                                    <li>
                                        <a href="#" title="Открыть">
                                            <i>
                                                <img src="images/icon__exit.png" alt="Открыть">
                                            </i>
                                            <span>Список</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>

                            <div class="box">

                                <div class="form_row">
                                    <div class="form_col form_col_50">
                                        <div class="form_inline mb_20">
                                            <div class="form_inline__label">Вид участника<sup class="color_red">*</sup></div>
                                            <div class="form_inline__input">
                                                <div class="select_01"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form_col form_col_100">
                                        <div class="form_inline mb_20">
                                            <div class="form_inline__label">Краткое наименование<sup class="color_red">*</sup></div>
                                            <div class="form_inline__input">
                                                <div class="input_01"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form_col form_col_100">
                                        <div class="form_inline mb_20">
                                            <div class="form_inline__label">Полное наименование<sup class="color_red">*</sup></div>
                                            <div class="form_inline__input">
                                                <div class="input_02"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="box">
                                <div class="box__heading"><span>ИНН, КПП (для российских лиц)</span></div>

                                <div class="form_row">
                                    <div class="form_col form_col_50">
                                        <div class="form_inline mb_20">
                                            <div class="form_inline__label">ИНН</div>
                                            <div class="form_inline__input">
                                                <div class="input_03"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_col form_col_50">
                                        <div class="form_inline mb_20">
                                            <div class="form_inline__label">КПП</div>
                                            <div class="form_inline__input">
                                                <div class="input_04"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div class="box">
                                <div class="box__heading"><span>Паспортные данные (для физический лиц)</span></div>

                                <div class="form_row">
                                    <div class="form_col form_col_50">
                                        <div class="form_inline mb_20">
                                            <div class="form_inline__label">Серия паспорта</div>
                                            <div class="form_inline__input">
                                                <div class="input_05"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_col form_col_50">
                                        <div class="form_inline mb_20">
                                            <div class="form_inline__label">Номер паспорта</div>
                                            <div class="form_inline__input">
                                                <div class="input_06"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_col">
                                        <div class="form_inline mb_20">
                                            <div class="form_inline__label">Выдан</div>
                                            <div class="form_inline__input">
                                                <div class="input_07"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="box">
                                <div class="box__heading"><span>Телефон, факс, e-mail, сайт</span></div>

                                <div class="form_row">
                                    <div class="form_col form_col_50">
                                        <div class="form_inline mb_20">
                                            <div class="form_inline__label">Телефон</div>
                                            <div class="form_inline__input">
                                                <div class="input_08"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_col form_col_50">
                                        <div class="form_inline mb_20">
                                            <div class="form_inline__label">Факс</div>
                                            <div class="form_inline__input">
                                                <div class="input_09"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_col form_col_50">
                                        <div class="form_inline mb_20">
                                            <div class="form_inline__label">E-mail</div>
                                            <div class="form_inline__input">
                                                <div class="input_10"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_col form_col_50">
                                        <div class="form_inline mb_20">
                                            <div class="form_inline__label">Сайт</div>
                                            <div class="form_inline__input">
                                                <div class="input_11"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="box">
                                <div class="form_row">
                                    <div class="form_col">
                                        <div class="form_inline mb_20">
                                            <div class="form_inline__label">Адрес места нахождения</div>
                                            <div class="form_inline__input">
                                                <div class="input_12"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_col form_col_50">
                                        <div class="form_inline mb_20">
                                            <div class="form_inline__label">Страна<sup class="color_red">*</sup></div>
                                            <div class="form_inline__input">
                                                <div class="select_02"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_col form_col_50">
                                        <div class="form_inline mb_20">
                                            <div class="form_inline__label">Город (населенный пункт)<sup class="color_red">*</sup></div>
                                            <div class="form_inline__input">
                                                <div class="input_13"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="box">
                                <div class="box__heading"><span>Виды и характер деятельности</span></div>

                                <div class="textarea_01"></div>
                            </div>

                        </div>

                    </div>
                </section>

            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

    <script>

        $(".input_01").dxTextBox({});

        $(".input_02").dxTextBox({});

        $(".select_01").dxSelectBox({
            placeholder: "Российский участник",
            "dataSource": [
                "Российский участник",
                "Иностранный участник участник"
            ]
        });

        $(".input_03").dxTextBox({});

        $(".input_04").dxTextBox({});

        $(".input_05").dxTextBox({});

        $(".input_06").dxTextBox({});

        $(".input_07").dxTextBox({});

        $(".input_08").dxTextBox({});

        $(".input_09").dxTextBox({});

        $(".input_10").dxTextBox({});

        $(".input_11").dxTextBox({});

        $(".input_12").dxTextBox({
            value: "625048, Тюменская область, г. Тюмень, улица 50 лет Октября, дом 14"
        });

        $(".input_13").dxTextBox({});

        $(".select_02").dxSelectBox({
            placeholder: "Страна",
            "dataSource": [
                "Россия",
                "Франция",
                "Германия",
                "Норвегия",
                "Финляндия",
                "Эстония"
            ]
        });

        $(".textarea_01").dxTextArea({
            value: "Объект экспертизы представляет собой кабельный удлиннитель PEDMT, состоящий из 3-х медных проводников и кабельной муфты, расчитанный на напряжение 4кВ",
            height: 120
        });

    </script>

    </body>
</html>
