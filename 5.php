<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <section class="main">

                <?php include('inc/sidebar.inc.php') ?>

                <section class="content">
                    <div class="content__wrap">

                        <div class="inner">

                            <h1>Список товаров</h1>

                            <nav class="menu">
                                <ul>
                                    <li>
                                        <a href="#" title="Открыть">
                                            <i>
                                                <img src="images/icon__open.png" alt="Открыть">
                                            </i>
                                            <span>Открыть</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="Открыть">
                                            <i>
                                                <img src="images/icon__add.png" alt="Добавить">
                                            </i>
                                            <span>Добавить</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="Удалить">
                                            <i>
                                                <img src="images/icon__remove.png" alt="Удалить">
                                            </i>
                                            <span>Удалить</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="Анализ">
                                            <i>
                                                <img src="images/icon__analysis.png" alt="Анализ">
                                            </i>
                                            <span>Анализ</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>

                            <div class="content__main">

                                <div class="data_table mb_40"></div>

                                <div class="box">

                                    <div class="box__heading"><span>Общие сведения об объектах экспертизы</span></div>
                                    <div class="form_inline mb_20">
                                        <div class="form_inline__label label_long">Область применения объектов экспертизы</div>
                                        <div class="form_inline__input">
                                            <div class="textarea_01"></div>
                                        </div>
                                    </div>
                                    <div class="form_inline mb_20">
                                        <div class="form_inline__label label_long"">Назначение объектов экспертизы</div>
                                        <div class="form_inline__input">
                                            <div class="textarea_02"></div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                </section>

            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

        <script>

            var employees = [
                { id: '1', name: "Кабельный удлинниетль PEDMT, из 3-х медных проводников: Американского Калибра Проводов", data: ""},
                { id: '2', name: "Электродвигатель асинхронный переменного тока 117, 17, VPEDMT, 3-х фазный, 17-ти секционный, погружной...", data: ""},
                { id: '3', name: "Электродвигатель асинхронный переменного тока 117, 20, VPEDMT, 3-х фазный, погружной (ПЭД) УЭЦН...", data: ""},
                { id: '4', name: "Оборудование фильтровальное: модульная гидрозащита (протектор) GZMT 10, BPBSL-UT, 540/103...", data: ""},
                { id: '5', name: "Оборудование фильтровальное: модульная гидрозащита (протектор) GZMT 10, BPBSL-UT, 103/103...", data: ""},
                { id: '6', name: "Изделия из пластмасс: уплотнительная прокладка, для изоляции кабельных выводов в соединительной...", data: ""},
                { id: '7', name: "Части жидностных насосов: рабочее колесо (крыльчатка) насосоа DN1750 установок электрических...", data: ""},
                { id: '8', name: "Машины и механические устройства имеющие индивидуальные функции: стенд цепной, механический, моде...", data: ""}
            ];

            $(function(){
                var i1 = 0;

                $(".data_table").dxTreeList({
                    dataSource: employees,
                    columnAutoWidth: true,
                    wordWrapEnabled: true,
                    showBorders: true,
                    selection: {
                        mode: "single",
                        recursive: false
                    },
                    columns: [
                        {
                            dataField: "id",
                            caption: "№", width: 55
                        },
                        { dataField: "name", caption: "Коммерческое наименование" },
                        {
                            dataField: "data",
                            caption: "Данные",
                            width: 100,
                            alignment: 'center',
                            cellTemplate: function(container, options) {

                                i1 = i1 + 1;
                                var checkboxOne = 'checkbox_' + i1;
                                var checkOne = '.' + checkboxOne;

                                container
                                    .append($("<div>", { "class": checkboxOne}))
                                    .append("\n");
                                $(checkOne).dxCheckBox({});
                            },
                        },
                    ]
                });
            });

            $(".textarea_01").dxTextArea({
                height: 80
            });

            $(".textarea_02").dxTextArea({
                height: 80
            });

        </script>

    </body>
</html>
