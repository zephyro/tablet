<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <section class="main">

                <?php include('inc/sidebar.inc.php') ?>

                <section class="content">
                    <div class="content__wrap">

                        <div class="inner">

                            <ul class="breadcrumbs">
                                <li><a href="#">Товары</a></li>
                                <li><a href="#">Объект № 1</a></li>
                                <li><a href="#">Сведения о товаре</a></li>
                            </ul>

                            <nav class="menu menu_border">
                                <ul>
                                    <li>
                                        <a href="#" title="Открыть">
                                            <i>
                                                <img src="images/icon__exit.png" alt="Открыть">
                                            </i>
                                            <span>Список</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>

                            <div class="box">
                                <div class="box__heading"><span>Регистрационные данные</span></div>

                                <div class="form_row">

                                    <div class="form_col">
                                        <div class="form_inline mb_20">
                                            <div class="form_inline__label">Коммерческое наименование<sup class="color_red">*</sup></div>
                                            <div class="form_inline__input">
                                                <div class="input_01"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form_col form_col_50">
                                        <div class="form_inline mb_20">
                                            <div class="form_inline__label">Изготовитель<sup class="color_red">*</sup></div>
                                            <div class="form_inline__input">
                                                <div class="input_02"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form_col form_col_100">
                                        <div class="form_inline mb_20">
                                            <div class="form_inline__label">Паспортное наименование</div>
                                            <div class="form_inline__input">
                                                <div class="textarea_01"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form_col form_col_100">
                                        <div class="form_inline mb_20">
                                            <div class="form_inline__label">Обозначение</div>
                                            <div class="form_inline__input">
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-7 col-xl-8">
                                                        <div class="input_03"></div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-5 col-xl-4">
                                                        <div class="checkbox_01"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form_col form_col_100">
                                        <div class="form_inline mb_20">
                                            <div class="form_inline__label">Категория<sup class="color_red">*</sup></div>
                                            <div class="form_inline__input">
                                                <div class="select_01"></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="box">
                                <div class="box__heading"><span>Код ТН ВЭД, номер CAS</span></div>

                                <div class="form_row">
                                    <div class="form_col form_col_50">
                                        <div class="form_inline mb_20">
                                            <div class="form_inline__label">Код ТН ВЭД<span class="color_red">*</span></div>
                                            <div class="form_inline__input">
                                                <div class="input_04"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form_col form_col_50">
                                        <div class="form_inline mb_20">
                                            <div class="form_inline__label">Номер CAS</div>
                                            <div class="form_inline__input">
                                                <div class="input_05"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="box">
                                <div class="box__heading"><span>Область применения</span></div>
                                <div class="textarea_02"></div>
                            </div>

                            <div class="box">
                                <div class="box__heading"><span>Назначение</span></div>
                                <div class="textarea_03"></div>
                            </div>

                            <div class="box">
                                <div class="box__heading"><span>Техническое описание<sup class="color_red">*</sup></span></div>
                                <div class="textarea_04"></div>
                            </div>

                            <div class="box">
                                <div class="box__heading"><span>Особенности (признаки возможного использования в контролируемых целях)</span></div>
                                <div class="textarea_05"></div>
                            </div>

                            <div class="text-right color_green">Выбрать из списка документов</div>

                            <div class="box mb_5">
                                <div class="box__heading"><span>Документы</span></div>
                                <div class="inline_block">
                                    <div class="inline_block__item">
                                        <div class="form_inline form_inline_float form_inline_long mb_10">
                                            <div class="form_inline__label">Наименование документа</div>
                                            <div class="form_inline__input">
                                                <div class="input_06"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inline_block__item">
                                        <div class="form_inline form_inline_float form_inline_long mb_10">
                                            <div class="form_inline__label">Рег. №</div>
                                            <div class="form_inline__input">
                                                <div class="input_07"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inline_block__item">
                                        <div class="form_inline form_inline_float mb_10">
                                            <div class="form_inline__label">Рег. дата</div>
                                            <div class="form_inline__input">
                                                <div class="date_01"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inline_block__item">
                                        <div class="upload_01"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="mb_30">
                                <a href="#" class="btn_text">Добавить</a>
                            </div>

                        </div>

                    </div>
                </section>

            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

        <script>

            $(".input_01").dxTextBox({
                value: 'Кабельный удлиннитель PEDMT, из 3-х медных проводников: Американского Калибра Проводов #6AWG (и\\диаметром 4,1155мм), на напряжение 4'
            });

            $(".input_02").dxTextBox({
                value: '1'
            });

            $(".textarea_01").dxTextArea({
                height: 80
            });

            $(".input_03").dxTextBox({
                placeholder: "123456789",
            });

            $(".select_01").dxSelectBox({
                placeholder: "",
                "dataSource": [
                    "1 - оборудование серийного производства",
                    "2 - оборудование серийного производства",
                    "3 - оборудование серийного производства",
                    "4 - оборудование серийного производства",
                    "5 - оборудование серийного производства",
                    "1 - оборудование серийного производства"
                ]
            });

            $(".checkbox_01").dxCheckBox({
                text: "Обозначение отсутствует",
            });


            $(".input_04").dxTextBox({
                value: '12345675'
            });

            $(".input_05").dxTextBox({});


            $(".textarea_02").dxTextArea({
                height: 80
            });


            $(".textarea_03").dxTextArea({
                height: 60
            });


            $(".textarea_04").dxTextArea({
                height: 80
            });


            $(".textarea_05").dxTextArea({
                height: 80
            });

            $(".input_06").dxTextBox({});

            $(".input_07").dxTextBox({});

            $(".date_01").dxDateBox({});

            $(".upload_01").dxFileUploader({
                selectButtonText: 'Файл',
                showFileList: false,
                labelText: ''
            });

        </script>

    </body>
</html>
