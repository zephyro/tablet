<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <section class="main">

                <?php include('inc/sidebar.inc.php') ?>

                <section class="content">
                    <div class="content__wrap">

                        <div class="inner">

                            <h1>Анализ товаров</h1>

                            <nav class="menu">
                                <ul>
                                    <li>
                                        <a href="#" title="Открыть">
                                            <i>
                                                <img src="images/icon__open.png" alt="Открыть">
                                            </i>
                                            <span>Открыть</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="Сформировать заключение">
                                            <i>
                                                <img src="images/icon__generate.png" alt="Открыть">
                                            </i>
                                            <span>Сформировать заключение</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>

                        <div class="data_table mb_40"></div>

                    </div>
                </section>

            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

        <script>
            var employees = [
                { id: "1", name: "Кабельный удлинниетль PEDMT, из 3-х медных проводник...", stop: "", control: "", list: ""},
                { id: "2", name: "Электродвигатель асинхронный переменного тока 117, 17... ", stop: "", control: "", list: ""},
                { id: "3", name: "Электродвигатель асинхронный переменного тока 117, 20...", stop: "", control: "", list: ""},
                { id: "4", name: "Кабельный удлинниетль PEDMT, из 3-х медных проводник...", stop: "", control: "", list: ""},
                { id: "5", name: "Электродвигатель асинхронный переменного тока 117, 17... ", stop: "", control: "", list: ""},
                { id: "6", name: "Электродвигатель асинхронный переменного тока 117, 20...", stop: "", control: "", list: ""},
                { id: "7", name: "Кабельный удлинниетль PEDMT, из 3-х медных проводник...", stop: "", control: "", list: ""},
                { id: "8", name: "Электродвигатель асинхронный переменного тока 117, 17... ", stop: "", control: "", list: ""},
                { id: "9", name: "Электродвигатель асинхронный переменного тока 117, 20...", stop: "", control: "", list: ""},
            ];

            $(function(){

                var i1 = 0,
                    i2 = 0,
                    i3 = 0;

                $(".data_table").dxTreeList({
                    dataSource: employees,
                    columnAutoWidth: true,
                    wordWrapEnabled: true,
                    showBorders: true,
                    selection: {
                        mode: "multiple",
                        recursive: false
                    },
                    columns: [
                        { dataField: "id", caption: "", width: 70 },
                        { dataField: "name", caption: "Коммерческое наименование" },
                        {
                            dataField: "stop",
                            caption: "Запреты",
                            alignment: 'center',
                            cellTemplate: function(container, options) {

                                i1 = i1 + 1;
                                var checkboxOne = 'checkbox_' + i1;
                                var checkOne = '.' + checkboxOne;

                                container
                                    .append($("<div>", { "class": checkboxOne}))
                                    .append("\n");
                                $(checkOne).dxCheckBox({});
                            },
                        },
                        {
                            dataField: "control",
                            caption: "На контроле",
                            alignment: 'center',
                            cellTemplate: function(container, options) {

                                i2 = i2 + 1;
                                var checkboxTwo = 'checkbox_' + i2;
                                var checkTwo = '.' + checkboxTwo;

                                container
                                    .append($("<div>", { "class": checkboxTwo}))
                                    .append("\n");
                                $(checkTwo).dxCheckBox({});
                            },
                        },
                        {
                            dataField: "list",
                            caption: "Контр. списки",
                            alignment: 'center',
                            cellTemplate: function(container, options) {

                                i3 = i3 + 1;
                                var checkboxThree = 'checkbox_' + i3;
                                var checkThree = '.' + checkboxThree;

                                container
                                    .append($("<div>", { "class": checkboxThree}))
                                    .append("\n");
                                $(checkThree).dxCheckBox({});
                            },

                        }
                    ]
                });
            });


        </script>

    </body>
</html>

