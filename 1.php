<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="page">

            <div class="top_line">
                <div class="top_line__toggle"></div>
                <div class="top_line__heading">ПЛАН РАБОТ</div>
            </div>

            <section class="main">
                <div class="content">
                    <div class="container-fluid">

                        <ul class="content_nav">
                            <li>
                                <div class="content_nav__create"></div>
                            </li>
                            <li>
                                <div class="content_nav__open"></div>
                            </li>
                        </ul>

                        <div class="content__row">
                            <div class="content__main">
                                <div class="box_responsive">
                                    <div class="data_table"></div>
                                </div>
                            </div>
                            <div class="content__three">
                                <div class="tree">
                                    <div class="tree__heading">
                                        <div class="tree__heading_text">Переход к разделу работу</div>
                                        <div class="tree__heading_button"></div>
                                    </div>
                                    <div class="tree__content">
                                        <div class="data_tree"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

        <script>

            $(".top_line__toggle").dxButton({
                "text": "Содержание"
            });

            $(".content_nav__create").dxButton({
                "text": "Создать"
            });

            $(".content_nav__open").dxButton({
                "text": "Открыть"
            });

            $(".tree__heading_button").dxButton({
                "text": ""
            });

            var employees = [
                {
                    id: "1",
                    idx: "6740",
                    name: 'Станочное оборудование',
                    status: 'Утверждение',
                    user: 'Курдюкова Г. В.',
                    boss: 'Усанов В. В.',
                    text: '',
                    nav: '',
                },
                {
                    id: "2",
                    idx: "6740",
                    name: 'Станочное оборудование',
                    status: 'Утверждение',
                    user: 'Курдюкова Г. В.',
                    boss: 'Усанов В. В.',
                    text: '',
                    nav: '',
                },
                {
                    id: "3",
                    idx: "6740",
                    name: 'Станочное оборудование',
                    status: 'Утверждение',
                    user: 'Курдюкова Г. В.',
                    boss: 'Усанов В. В.',
                    text: '',
                    nav: '',
                },
                {
                    id: "4",
                    idx: "6740",
                    name: 'Станочное оборудование',
                    status: 'Утверждение',
                    user: 'Курдюкова Г. В.',
                    boss: 'Усанов В. В.',
                    text: '',
                    nav: '',
                },
                {
                    id: "5",
                    idx: "6740",
                    name: 'Станочное оборудование',
                    status: 'Утверждение',
                    user: 'Курдюкова Г. В.',
                    boss: 'Усанов В. В.',
                    text: '',
                    nav: '',
                },

            ];

            $(function(){

                $(".data_table").dxDataGrid({
                    dataSource: employees,
                    columnAutoWidth: true,
                    wordWrapEnabled: true,
                    showBorders: true,
                    selection: {
                        mode: "single",
                        recursive: false
                    },
                    columns: [
                        {
                            dataField: 'idx',
                            width: 120,
                            caption: 'Работа (ID)'
                        },
                        {
                            dataField: 'name',
                            width: 280,
                            caption: 'Название'

                        },
                        {
                            dataField: 'status',
                            width: 170,
                            caption: 'Состояние'

                        },
                        {
                            dataField: 'user',
                            width: 170,
                            caption: 'Исполнитель'

                        },
                        {
                            dataField: 'boss',
                            width: 170,
                            caption: 'Руководитель'

                        },
                        {
                            dataField: 'text',
                            caption: ''

                        },
                        {
                            dataField: 'nav',
                            width: 55,
                            caption: ''
                        },
                    ]
                });
            });


            var tree = [
                {
                    "id": "1",
                    "text": "Stores",
                    "expanded": true,
                    "items": [
                        {
                            "id": "1_1",
                            "text": "Super Mart of the West",
                            "expanded": true,
                            "items": [
                                {
                                    "id": "1_1_1",
                                    "text": "Video Players",
                                    "items": [
                                        {
                                            "id": "1_1_1_1",
                                            "text": "HD Video Player",
                                            "price": 220,
                                            "image": "images/products/1.png"
                                        },
                                        {
                                            "id": "1_1_1_2",
                                            "text": "SuperHD Video Player",
                                            "image": "images/products/2.png",
                                            "price": 270
                                        }
                                    ]
                                },
                                {
                                    "id": "1_1_2",
                                    "text": "Televisions",
                                    "expanded": true,
                                    "items": [
                                        {
                                            "id": "1_1_2_1",
                                            "text": "SuperLCD 42",
                                            "image": "images/products/7.png",
                                            "price": 1200
                                        },
                                        {
                                            "id": "1_1_2_2",
                                            "text": "SuperLED 42",
                                            "image": "images/products/5.png",
                                            "price": 1450
                                        },
                                        {
                                            "id": "1_1_2_3",
                                            "text": "SuperLED 50",
                                            "image": "images/products/4.png",
                                            "price": 1600
                                        },
                                        {
                                            "id": "1_1_2_4",
                                            "text": "SuperLCD 55",
                                            "image": "images/products/6.png",
                                            "price": 1350
                                        },
                                        {
                                            "id": "1_1_2_5",
                                            "text": "SuperLCD 70",
                                            "image": "images/products/9.png",
                                            "price": 4000
                                        }
                                    ]
                                },
                                {
                                    "id": "1_1_4",
                                    "text": "Projectors",
                                    "items": [
                                        {
                                            "id": "1_1_4_1",
                                            "text": "Projector Plus",
                                            "image": "images/products/14.png",
                                            "price": 550
                                        },
                                        {
                                            "id": "1_1_4_2",
                                            "text": "Projector PlusHD",
                                            "image": "images/products/15.png",
                                            "price": 750
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "1_2",
                            "text": "Braeburn",
                            "items": [
                                {
                                    "id": "1_2_1",
                                    "text": "Video Players",
                                    "items": [
                                        {
                                            "id": "1_2_1_1",
                                            "text": "HD Video Player",
                                            "image": "images/products/1.png",
                                            "price": 240
                                        },
                                        {
                                            "id": "1_2_1_2",
                                            "text": "SuperHD Video Player",
                                            "image": "images/products/2.png",
                                            "price": 300
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]

            $(function(){
                $(".data_tree").dxTreeView({
                    "dataSource": tree
                });
            });

        </script>

    </body>
</html>

