<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <section class="main">

                <?php include('inc/sidebar.inc.php') ?>

                <section class="content">
                    <div class="content__wrap">

                        <div class="inner">
                            <h1>Сведения о работе</h1>

                            <nav class="menu menu_border">
                                <ul>
                                    <li>
                                        <a href="#" title="Участники">
                                            <i>
                                                <img src="images/icon__users.png" alt="Открыть">
                                            </i>
                                            <span>Участники</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>

                            <div class="box">
                                <div class="box__heading"><span>Название работы</span></div>
                                <div class="input_01"></div>
                            </div>

                            <div class="box">
                                <div class="box__heading"><span>Направление перемещения</span></div>
                                <div class="form_wrap">
                                    <div class="select_01"></div>
                                </div>
                            </div>

                            <div class="box">
                                <div class="box__heading"><span>Страна назначения (отправления)</span></div>
                                <div class="form_wrap">
                                    <div class="select_02"></div>
                                </div>
                            </div>

                            <div class="box mb_5">
                                <div class="box__heading"><span>Документы - основание внешнеэкономической операции</div>

                                <div class="inline_block">
                                    <div class="inline_block__item">
                                        <div class="form_inline form_inline_float form_inline_long mb_10">
                                            <div class="form_inline__label">Наименование документа</div>
                                            <div class="form_inline__input">
                                                <div class="input_01"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inline_block__item">
                                        <div class="form_inline form_inline_float form_inline_long mb_10">
                                            <div class="form_inline__label">Рег. №</div>
                                            <div class="form_inline__input">
                                                <div class="input_02"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inline_block__item">
                                        <div class="form_inline form_inline_float mb_10">
                                            <div class="form_inline__label">Рег. дата</div>
                                            <div class="form_inline__input">
                                                <div class="date_01"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="inline_block__item">
                                        <div class="upload_01"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="mb_30">
                                <a href="#" class="btn_text">Добавить</a>
                            </div>

                            <div class="box">
                                <div class="box__heading"><span>Описание внешнеэкономической операции<sup class="color_red">*</sup></span></div>
                                <div class="textarea_01"></div>
                            </div>

                            <div class="box">
                                <div class="box__heading"><span>Содержание операции</span></div>
                                <div class="textarea_02"></div>
                            </div>

                        </div>

                    </div>
                </section>

            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

        <script>

            $(".input_01").dxTextBox({});

            $(".input_02").dxTextBox({});

            $(".input_03").dxTextBox({});

            $(".date_01").dxDateBox({});

            $(function(){
                $(".select_01").dxSelectBox({
                    placeholder: "Укажите направление",
                    "dataSource": [
                        "Экспорт товара из Российской федерации",
                        "Импорт товара из Российской федерации",
                        "Перемещение внутри Российской федерации"
                    ]
                });
            });

            $(function(){
                $(".select_02").dxSelectBox({
                    placeholder: "Страна",
                    "dataSource": [
                        "Колумбия",
                        "Россия",
                        "Финляндия",
                        "Германия",
                        "Испания",
                        "Польша",
                        "Латвия"
                    ]
                });
            });

            $(".textarea_01").dxTextArea({
                placeholder: "Постановка оборудования нефтедобычи",
                height: 160
            });

            $(".textarea_02").dxTextArea({
                value: "Экспорт товара из Российской федерации",
                height: 175
            });

            $(".upload_01").dxFileUploader({
                selectButtonText: 'Файл',
                showFileList: false,
                labelText: ''
            });

        </script>

    </body>
</html>
