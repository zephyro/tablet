<!doctype html>
<html class="no-js" lang="">

    <?php include('inc/head.inc.php') ?>

    <body>

        <div class="page">

            <?php include('inc/header.inc.php') ?>

            <section class="main">

                <?php include('inc/sidebar.inc.php') ?>

                <section class="content">
                    <div class="content__wrap">

                        <div class="inner">

                            <h1>Анализ иностранных участников</h1>

                            <nav class="menu">
                                <ul>
                                    <li>
                                        <a href="#" title="Открыть">
                                            <i>
                                                <img src="images/icon__exit.png" alt="Открыть">
                                            </i>
                                            <span>Список</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="Анализ товаров">
                                            <i>
                                                <img src="images/icon__analysis_loop.png" alt="Анализ товаров">
                                            </i>
                                            <span>Анализ товаров</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>

                            <div class="data_table mb_40"></div>

                        </div>

                    </div>
                </section>

            </section>

            <?php include('inc/footer.inc.php') ?>

        </div>

        <?php include('inc/scripts.inc.php') ?>

        <script>
            var employees = [
                { id: "1", name: "Общество с ограниченной ответсвенностью \"Технологическая компания Шлюмберже\"", stop: "", control: ""},
                { id: "2", name: "Общество с ограниченной ответсвенностью \"Технологическая компания Шлюмберже\"", stop: "", control: ""},
            ];

            $(function(){

                var i1 = 0,
                    i2 = 0,
                    i3 = 0;

                $(".data_table").dxTreeList({
                    dataSource: employees,
                    columnAutoWidth: true,
                    wordWrapEnabled: true,
                    showBorders: true,
                    selection: {
                        mode: "multiple",
                        recursive: false
                    },
                    columns: [
                        { dataField: "id", caption: "", width: 70 },
                        { dataField: "name", caption: "Полное наименование" },
                        {
                            dataField: "stop",
                            caption: "Запреты",
                            alignment: 'center',
                            cellTemplate: function(container, options) {

                                i1 = i1 + 1;
                                var checkboxOne = 'checkbox_' + i1;
                                var checkOne = '.' + checkboxOne;

                                container
                                    .append($("<div>", { "class": checkboxOne}))
                                    .append("\n");
                                $(checkOne).dxCheckBox({});
                            },
                        },
                        {
                            dataField: "control",
                            caption: "На контроле",
                            alignment: 'center',
                            cellTemplate: function(container, options) {

                                i2 = i2 + 1;
                                var checkboxTwo = 'checkbox_' + i2;
                                var checkTwo = '.' + checkboxTwo;

                                container
                                    .append($("<div>", { "class": checkboxTwo}))
                                    .append("\n");
                                $(checkTwo).dxCheckBox({});
                            },
                        }
                    ]
                });
            });


        </script>

    </body>
</html>
